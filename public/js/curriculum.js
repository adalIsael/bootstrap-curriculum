$(document).ready(function() {
  var getUserInfo = {
    url: 'https://randomuser.me/api/?gender=male',
    dataType: 'json'
  };

  var $title = $('.js-title');
  var copy = '.js-copy';

  $title.click(function() {
      $(this).next(copy).slideToggle();
      $(this).parent().siblings().children().next().slideUp();
      return false;
  });
});
